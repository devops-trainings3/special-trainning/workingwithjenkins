pipeline {
    agent {
        docker {
            image 'node:lts'
        }
    }

    stages {

        stage("Git Checkout"){
            steps{

                git "git -url "
            }
        }
     
        stage("Build Image"){
            steps{
                sh """
                 docker build -t myreactjsimageee:lts . 
                 docker run -d -p 4000:3000 myreactjsimageee 
                """
            }
        }
    }
}

pipeline {
    agent any

    stages {
        stage('Git Checkout ') {
            steps {
                git 'https://gitlab.com/devops-trainings3/reactjs-17-templates-jenkins.git'
                sh '''
                ls -lrt 
                '''
            }
        }

        stage('Workspace CleanUp'){
            steps{
                sh '''
                 docker stop  react-testing-container || true
                 docker rm react-testing-container || true 
                '''
            }
        }

        stage("Build Docker Image "){
            steps{
                sh '''
                docker build -t testing-reactjs-image . 

                '''
            }
        }


        stage("Deploy in Built-in "){
            steps{
                sh '''
                    docker run -d -p 3333:80  --name react-testing-container testing-reactjs-image 
                '''
            }

        }
    }
}

// built-in will push its image to the nexus repo 
// slave-01 will pull the image 
// slave-01 will deploy the image 
pipeline {
    agent none 

    environment{
        IMAGE_NAME="testing-reactjs-image"
        TAG="latest"
        REGISTRY="registry-nexus.anuznomii.lol"
    }
    stages {
        stage('On built-in Node') {
            agent {  
                label 'built-in' // master 
            } 
            steps {
                 withCredentials([usernamePassword(credentialsId: 'nexus-repo-token', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {

                    sh '''
                        docker login -u $USERNAME -p $PASSWORD $REGISTRY
                        docker tag $IMAGE_NAME:$TAG $REGISTRY/$IMAGE_NAME:$BUILD_NUMBER
                        docker push $REGISTRY/$IMAGE_NAME:$BUILD_NUMBER

                    '''
    // some block
}
            }
        }

        stage("On Master-01 Node"){
            agent {
                label 'master-01'
            }
            steps{
                sh '''
                whoami 
                echo $WORKSPACE 
                '''
            }
        }
    }

}
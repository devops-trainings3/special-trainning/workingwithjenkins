
pipeline {
    agent any

    stages {
        stage('Clone Git') {
            steps {
                
                sh ''' 
                git clone https://gitlab.com/devops-trainings3/reactjs-17-templates-jenkins
                ls -lrt 
                ''' 
                // git 'https://gitlab.com/devops-trainings3/reactjs-17-templates-jenkins'
            }
        }
        stage("Build Image "){
            steps{
                dir("reactjs-17-templates-jenkins"){
                     sh '''
                     ls -lrt 
                     docker build -t reactjsssimageee .
                '''
                }
               
            }
        }

        stage("Deploy In Built-in"){
            steps{
                sh '''
                    docker run -d -p 3434:80 reactjsssimageee
                '''
            }
        }
    }

// after all the stages
    post{
        always{
            cleanWs()
        }
    }
}

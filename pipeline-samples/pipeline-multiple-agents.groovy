pipeline {
    agent none 

    stages {
        stage('On built-in Node') {
            agent { 
                label 'built-in'
            }
            steps {
                 sh '''
                 whoami 
                 pwd 
                 echo $WORKSPACE
                 '''
            }
        }

        stage("On Master-01 Node"){
            agent {
                label 'master-01'
            }
            steps{
                sh '''
                whoami 
                echo $WORKSPACE 
                '''
            }
        }
    }

}
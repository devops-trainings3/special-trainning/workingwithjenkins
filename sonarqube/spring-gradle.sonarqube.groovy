pipeline {
    agent any
    tools{
        gradle "Gradle"
    }
    environment{
        projectName = "spring_gradle_forked"
    }
    stages {
        stage('Git Checkout') {
            steps {
                git "https://gitlab.com/devops-trainings3/special-trainning/basic-springboot-project.git"
            }
        }

        stage("Install Dependencies and Build "){
            steps{
                sh """
                ./gradlew clean build
                """
            }
        }
          
        stage("Sonarqube Analysis "){
            steps{
                script {
                    // Configure the SonarQube server details
                    def scannerHome = tool 'sonar-name'; 
        
                     withSonarQubeEnv('sonar') {
                        sh "${tool("sonar-name")}/bin/sonar-scanner -Dsonar.projectKey=${projectName} -Dsonar.projectName=${projectName} -Dsonar.sources=src/main/java -Dsonar.java.binaries=build/classes/java/main -Dsonar.java.libraries=build/libs/*.jar" 
                      
           
                    }
                }
            }
        }


        
    }
}

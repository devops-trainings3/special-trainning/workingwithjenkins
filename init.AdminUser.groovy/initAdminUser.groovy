import jenkins.model.*
import hudson.security.*

def instance = Jenkins.instance
def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin", "password")
instance.setSecurityRealm(hudsonRealm)
instance.save()

FROM jenkins/jenkins:jdk17

USER root

# Ensure necessary binaries are accessible
RUN apt-get update && \
    apt-get install -y openjdk-17-jre && \
    apt-get clean
RUN apt-get update && \
    apt-get install -y ansible && \
    apt-get clean